/// xxHash64 module implementation
///
/// Specification : https://github.com/Cyan4973/xxHash/blob/dev/doc/xxhash_spec.md#xxh64-algorithm-description
///
/// repo: https://gitlab.com/kurdy/motoko-xxhash
///
/// License: BSD 3-Clause
import Nat64 "mo:base/Nat64";
import Nat32 "mo:base/Nat32";
import Nat8 "mo:base/Nat8";
import Nat "mo:base/Nat";
import Iter "mo:base/Iter";
import Buffer "mo:base/Buffer";
import Array "mo:base/Array";
import Debug "mo:base/Debug";
import Text "mo:base/Text";
import Option "mo:base/Option";
import Result "mo:base/Result";
import Bool "mo:base/Bool";
import Blob "mo:base/Blob";


module {

  type Result<T,E> = Result.Result<T,E>;

  /*
    * These constants are prime numbers, and feature a good mix of bits 1 and 0,
    * neither too regular, nor too dissymmetric. These properties help dispersion capabilities.
    */
  let PRIME64_1 : Nat64 = 0x9E3779B185EBCA87;  // 0b1001111000110111011110011011000110000101111010111100101010000111
  let PRIME64_2 : Nat64 = 0xC2B2AE3D27D4EB4F;  // 0b1100001010110010101011100011110100100111110101001110101101001111
  let PRIME64_3 : Nat64 = 0x165667B19E3779F9;  // 0b0001011001010110011001111011000110011110001101110111100111111001
  let PRIME64_4 : Nat64 = 0x85EBCA77C2B2AE63;  // 0b1000010111101011110010100111011111000010101100101010111001100011
  let PRIME64_5 : Nat64 = 0x27D4EB2F165667C5;  // 0b0010011111010100111010110010111100010110010101100110011111000101 2_870_177_450_012_600_261
  let STRIPES_SIZE : Nat = 32;
  let LANE_SIZE : Nat = 8;

  public class XXH64 (seed : Nat64) {
      
    var acc1 : Nat64 = seed +% PRIME64_1 +% PRIME64_2;
    var acc2 : Nat64 = seed +% PRIME64_2;
    var acc3 : Nat64 = seed;
    var acc4 : Nat64 = seed -% PRIME64_1;
    var atLeastOneStripe : Bool = false;
    var dataSize : Nat = 0;

    /// Reset state of instance after interruption of update's or finalize  
    public func reset() {
      acc1 := seed +% PRIME64_1 +% PRIME64_2;
      acc2 := seed +% PRIME64_2;
      acc3 := seed;
      acc4 := seed -% PRIME64_1;
      dataSize := 0;
      atLeastOneStripe := false;
    };
    
    /// Update state with a block of data
    /// The data block must be divisible by 32. The remainder will be passed to the finalize method
    /// Limitation:
    /// * 3_000_000 bytes over that limit you may trap with exceeded the instruction limit for single message execution.
    /// * 2_097_152 bytes which is the maximum size of a parameter call outside of canister.
    /// * update should be call with update func
    /// From specification is __Step 2. Process stripes__
    public func update(data : [Nat8]) : Result<(),Text>{
      let size = Iter.size(Array.vals(data));
  
      // Test if the data block is at least sized to STRIPES_SIZE 
      if (size < STRIPES_SIZE) {
        return #err("The minimum size of block is " # Nat.toText(STRIPES_SIZE) # " bytes. You may use directly finalize.");
      };
      // Test if the data block is divisible by STRIPES_SIZE
      if(size % STRIPES_SIZE != 0){
        return #err("The size of the data block must be divisible by " # Nat.toText(STRIPES_SIZE) #  ". Use finalize to transmit the last incomplete block. (< " # Nat.toText(STRIPES_SIZE) # " bytes)");
      };

      atLeastOneStripe:= true;
      dataSize += size;
      // compute stripe of data Block 
      var cursor : Nat = 0;
      let lane = Buffer.Buffer<Nat8>(0);
      let stripe = Buffer.Buffer<[Nat8]>(0);
      let nbLanes = STRIPES_SIZE / LANE_SIZE;
      while(cursor < size) {
        let b = data[cursor];
        lane.add(b);
        if (lane.size() >= LANE_SIZE) {
          stripe.add(Buffer.toArray(lane));
          lane.clear();
        };
        if (stripe.size() >= nbLanes) {
          processStripe(Buffer.toArray(stripe));
          stripe.clear();
        };
        cursor+=1;
      };
      return #ok()
    };

    /// Finalize the computation
    /// Limitation:
    /// * 2_950_000 bytes over that limit you may trap with error: exceeded the instruction limit for single message execution.
    /// * 2_097_152 bytes which is the maximum size of a parameter call outside of canister.
    /// If your data is larger than the limits below, you must split it into blocks divisible by 32 
    /// and proceed through update then finalize with the reminding data.
    public func finalize(data : [Nat8]) : Result<Nat64,Text>{
      var hash : Nat64 = 0;
      let size = Iter.size(Array.vals(data));
      var remindData: [Nat8] = [];
      let hasData : Bool = atLeastOneStripe or (size != 0); // atLeastOneStripe=true means update has been called before

      if(hasData) {
        // Test if data size is greater to a stripe size if yes we need to call update before
        if(size >= STRIPES_SIZE) {
          let dataBlock = Buffer.Buffer<Nat8>(0);
          let nbStripes = size / STRIPES_SIZE;
          let nbRemind = size % STRIPES_SIZE;
          let lastByte : Nat = (nbStripes * STRIPES_SIZE) - 1;
          for (i in Iter.range(0,lastByte)) {
            dataBlock.add(data[i]);
          };
          let result = update(Buffer.toArray(dataBlock));
          dataBlock.clear();
          switch result { 
            case (#err(m)) return #err("finalize error: " # m);
            case (#ok) ();
          };
          
          if(nbRemind != 0 ) {
            for (i in Iter.range(lastByte+1,size - 1)) {
              dataBlock.add(data[i]);
            };
            remindData := Buffer.toArray(dataBlock);
            dataBlock.clear();
          };
        } else {
          remindData := data;
        };

        // Accumulator convergence
        // From specification: __Step 3. Accumulator convergence__
        if(atLeastOneStripe) {
          hash := acc1 <<> 1;
          hash := hash +% (acc2 <<> 7);
          hash := hash +% (acc3 <<> 12);
          hash := hash +% (acc4 <<> 18);
          hash := mergeAccumulator(hash,acc1);
          hash := mergeAccumulator(hash,acc2);
          hash := mergeAccumulator(hash,acc3);
          hash := mergeAccumulator(hash,acc4);
        } else {
          hash := seed +% PRIME64_5;
        };

        // Add data size in byte to the hash
        // From specification:  __Step 4. Add input length__
        hash +%= Nat64.fromNat(dataSize + Iter.size(Array.vals(remindData)));

        // From specification:  __Step 5. Consume remaining input__
        hash := processRemindingData(hash,remindData);
      } else {
        // Data is empty
        hash := seed +% PRIME64_5;
      };
      reset();
      // __Step 6. Final mix (avalanche)__
      return #ok(avalanche(hash));
    };

    /*
     * Begin of private methods
     */
    private func avalanche (hash : Nat64) : Nat64 {
      var acc : Nat64 = hash;
      acc ^= acc >> 33;
      acc *%= PRIME64_2;
      acc ^= acc >> 29;
      acc *%= PRIME64_3;
      acc ^= acc >> 32;
      return acc;
    };

    // Step 2
    private func processStripe (stripe : [[Nat8]]) { 
      acc1 := round(acc1,toNat64(stripe[0]));
      acc2 := round(acc2,toNat64(stripe[1]));
      acc3 := round(acc3,toNat64(stripe[2]));
      acc4 := round(acc4,toNat64(stripe[3]));
    };

    // Step 2-3
    private func round(acc : Nat64, accN : Nat64) : Nat64 {
      var result = acc +% (accN *% PRIME64_2);
      result := (result <<> 31) *% PRIME64_1;
      return result; 
    };

    private func mergeAccumulator(acc : Nat64, accN : Nat64) : Nat64 {
      var result = acc ^ round(0,accN);
      result *%= PRIME64_1;
      result +%= PRIME64_4;
      return result;
    };

    // Step 5
    private func processRemindingData (hash : Nat64, data: [Nat8]) : Nat64 {
      var remindDataLen : Nat = Iter.size(Array.vals(data));
      if(remindDataLen == 0) {
        // Nothing todo
        return hash;
      };

      var result = hash;
      if(remindDataLen > STRIPES_SIZE){
        Debug.trap("Reminding data should be < " # Nat.toText(STRIPES_SIZE) # " bytes.");
      };

      let lane = Buffer.Buffer<Nat8>(0);
      var i : Nat = 0;
      while(remindDataLen >= LANE_SIZE){
        lane.add(data[i]);
        lane.add(data[i+1]);
        lane.add(data[i+2]);
        lane.add(data[i+3]);
        lane.add(data[i+4]);
        lane.add(data[i+5]);
        lane.add(data[i+6]);
        lane.add(data[i+7]);
        result := result ^ round(0,toNat64(Buffer.toArray(lane)));
        result := (result <<> 27) *% PRIME64_1;
        result +%= PRIME64_4;
        lane.clear();
        i+=LANE_SIZE;
        remindDataLen-=LANE_SIZE;
      };

      if(remindDataLen >= (LANE_SIZE/2)){
        lane.add(data[i]);
        lane.add(data[i+1]);
        lane.add(data[i+2]);
        lane.add(data[i+3]);
        result := result ^ (Nat64.fromNat(Nat32.toNat(toNat32(Buffer.toArray(lane)))) *% PRIME64_1);
        result := (result <<> 23) *% PRIME64_2;
        result +%= PRIME64_3;
        i+=LANE_SIZE/2;
        remindDataLen-=LANE_SIZE/2;
      };

      while(remindDataLen != 0){
        result := result ^ (Nat64.fromNat(Nat8.toNat(data[i])) *% PRIME64_5);
        result := (result <<> 11) *% PRIME64_1;
        i+=1;
        remindDataLen-=1;
      };
      return result;
    };

    private func toNat64 (lane : [Nat8]) : Nat64 {
      var result : Nat64 = 0;
      var shift : Nat64 = 0;
      for (byte in lane.vals()){
        result += Nat64.fromNat(Nat8.toNat(byte)) << shift;
        shift += 8;
      };
      return result;
    };  

    private func toNat32 (lane : [Nat8]) : Nat32 {
      var result : Nat32 = 0;
      var shift : Nat32 = 0;
      for (byte in lane.vals()){
        result += Nat32.fromNat(Nat8.toNat(byte)) << shift;
        shift += 8;
      };
      return result;
    }; 
  }; // End of class

  /// Quick and easy call for data size below < 2097152 seed 0 and trap if error.
  /// Can be use in query func
  public func hash(data : [Nat8]) : Nat64 {
    let hasher = XXH64(0);
    let r = hasher.finalize(data);
    switch r {
      case (#ok(h)) return h;
      case (#err(m)) Debug.trap(m);
    }; 
  };

  /// Quick and easy call for data size below < 2097152 seed 0 and trap if error.
  /// Can be use in query func
  public func textHash(text : Text) : Nat64 {
    let hasher = XXH64(0);
    let data = Blob.toArray(Text.encodeUtf8(text));  
    let r = hasher.finalize(data);
    switch r {
      case (#ok(h)) return h;
      case (#err(m)) Debug.trap(m);
    };
  };

};